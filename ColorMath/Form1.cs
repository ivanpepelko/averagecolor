﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ColorMath {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) {
            DialogResult r = openFileDialog1.ShowDialog();
            if (r == DialogResult.OK) {
                Bitmap b = new Bitmap(openFileDialog1.FileName);
                Color avg = ColorMath.getDominantColor(b);
                textBox1.Text = openFileDialog1.FileName;
                labelColor.BackColor = avg;
                labelR.Text = avg.R.ToString();
                labelG.Text = avg.G.ToString();
                labelB.Text = avg.B.ToString();
                pictureBox1.Image = new Bitmap(b, pictureBox1.Size);
            }
        }
    }
}
